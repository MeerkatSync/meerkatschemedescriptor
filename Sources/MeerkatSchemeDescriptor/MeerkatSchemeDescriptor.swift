//
//  MeerkatSchemeDescriptor.swift
//  
//
//  Created by Filip Klembara on 18/02/2020.
//

import Foundation
import MeerkatCore

public struct MeerkatSchemeDescriptor {
    private var scheme: [String: ClassAttributesDescription]

    public init(scheme: [ClassAttributesDescription] = []) {
        self.scheme = Dictionary(uniqueKeysWithValues: scheme.map { ($0.className, $0) })
    }

    public mutating func addClass(_ cls: ClassAttributesDescription) {
        scheme[cls.className] = cls
    }

    public func getData(for attribute: String, value: UpdateValue, in cls: String) throws -> Data? {
        guard let cls = scheme[cls] else {
            throw SchemeDescriptorError.unknownClass
        }
        return try cls.getData(for: attribute, value: value)
    }

    public func getValue(for attribute: String, from data: Data?, in cls: String, prefferedArrayRepresentation: ClassAttributesDescription.PrefferedArrayRepresentation = .array) throws -> UpdateValue {
        guard let cls = scheme[cls] else {
            throw SchemeDescriptorError.unknownClass
        }
        return try cls.getValue(for: attribute, from: data, prefferedArrayRepresentation: prefferedArrayRepresentation)
    }

//    enum RelationType {
//        case object
//        case array
//    }
//
//    func getRelatedClasses(for className: String) throws -> [(String, String, RelationType)] {
//        guard let cls = scheme[className] else {
//            throw SchemeDescriptorError.unknownClass
//        }
//        return cls.attributes.map { ($0, $1.relatedClass) }.compactMap { a, v in
//            guard let v = v else {
//                return nil
//            }
//            return (a, v.0, v.1)
//        }
//    }
}

public struct ClassAttributesDescription {
    public enum PrefferedArrayRepresentation {
        case arrayDiff
        case array
    }
    public enum AttributeType {
        case string
        case int
        case data
        case bool
        case uint
        case double
        case date
        case optionalString
        case optionalInt
        case optionalData
        case optionalBool
        case optionalUInt
        case optionalDouble
        case optionalObject(className: String)
        case optionalDate
        case array(className: String)

//        var relatedClass: (String, MeerkatSchemeDescriptor.RelationType)? {
//            switch self {
//            case .optionalObject(let className):
//                 return (className, .object)
//            case .array(let className):
//                return (className, .array)
//            default:
//                return nil
//            }
//        }

        public var isRelation: Bool {
            switch self {
            case .optionalObject, .array:
                return true
            default:
                return false
            }
        }

        public var isOptional: Bool {
            switch self {
            case .optionalString, .optionalInt, .optionalData, .optionalBool, .optionalUInt, .optionalDouble, .optionalObject, .optionalDate:
                return true
            default:
                return false
            }
        }

        public func getData(for value: UpdateValue) throws -> Data? {
            switch (value, self) {
            case let (.string(s), .string), let (.string(s), .optionalString):
                return s.data(using: .utf8)
            case (.int(let i), .int), (.int(let i), .optionalInt):
                return i.description.data(using: .utf8)
            case (.data(let d), .data), (.data(let d), .optionalData):
                return d
            case (.bool(let b), .bool), (.bool(let b), .optionalBool):
                return (b ? "true" : "false").data(using: .utf8)
            case (.uint(let i), .uint), (.uint(let i), .optionalUInt):
                return i.description.data(using: .utf8)
            case (.double(let i), .double), (.double(let i), .optionalDouble):
                return i.description.data(using: .utf8)
            case (.date(let d), .date), (.date(let d), .optionalDate):
                return d.timeIntervalSince1970.description.data(using: .utf8)
            case (.nil, .optionalString), (.nil, .optionalInt), (.nil, .optionalBool), (.nil, .optionalData), (.nil, .optionalUInt), (.nil, .optionalDouble), (.nil, .optionalObject), (.nil, .optionalDate):
                return nil
            case (.object(let id), .optionalObject):
                return id.data(using: .utf8)
            case (.array(let arr), .array):
                let encoder = JSONEncoder()
                return try encoder.encode(arr)
            case (.arrayDiff(let diff), .array):
                let encoder = JSONEncoder()
                return try encoder.encode(diff)
            default:
                throw SchemeDescriptorError.wrongType
            }
        }

        public func asUpdateValue(from odata: Data?, prefferedArrayRepresentation: PrefferedArrayRepresentation = .array) throws -> UpdateValue {
            if let data = odata {
                switch self {
                case .string, .optionalString:
                    guard let s = String(data: data, encoding: .utf8) else { throw SchemeDescriptorError.wrongType }
                    return .string(s)
                case .int, .optionalInt:
                    guard let s = String(data: data, encoding: .utf8) else { throw SchemeDescriptorError.wrongType }
                    guard let i = Int(s) else { throw SchemeDescriptorError.wrongType }
                    return .int(i)
                case .data, .optionalData:
                    return .data(data)
                case .bool, .optionalBool:
                    guard let s = String(data: data, encoding: .utf8) else { throw SchemeDescriptorError.wrongType }
                    guard ["true", "false"].contains(s) else { throw SchemeDescriptorError.wrongType }
                    return .bool(s == "true")
                case .uint, .optionalUInt:
                    guard let s = String(data: data, encoding: .utf8) else { throw SchemeDescriptorError.wrongType }
                    guard let i = UInt(s) else { throw SchemeDescriptorError.wrongType }
                    return .uint(i)
                case .double, .optionalDouble:
                    guard let s = String(data: data, encoding: .utf8) else { throw SchemeDescriptorError.wrongType }
                    guard let d = Double(s) else { throw SchemeDescriptorError.wrongType }
                    return .double(d)
                case .date, .optionalDate:
                    guard let s = String(data: data, encoding: .utf8) else { throw SchemeDescriptorError.wrongType }
                    guard let d = Double(s) else { throw SchemeDescriptorError.wrongType }
                    let date = Date(timeIntervalSince1970: d)
                    return .date(date)
                case .optionalObject:
                    guard let s = String(data: data, encoding: .utf8) else { throw SchemeDescriptorError.wrongType }
                    return .object(id: s)
                case .array:
                    let decoder = JSONDecoder()
                    if let decDiff = try? decoder.decode(UpdateValue.ArrayDiff.self, from: data) {
                        return prefferedArrayRepresentation == .array ? .array(decDiff.original.applying(decDiff.difference)!) : .arrayDiff(decDiff)
                    } else {
                        let array = try decoder.decode([ObjectID].self, from: data)
                        return prefferedArrayRepresentation == .array ? .array(array) : .arrayDiff(.init(original: [], new: array))
                    }
                }
            } else {
                guard isOptional else {
                    throw SchemeDescriptorError.notOptional
                }
                return .nil
            }
        }
    }
    public let className: String
    public let attributes: [String: AttributeType]

    public init(className: String, attributes: [String: AttributeType]) {
        self.className = className
        self.attributes = attributes
    }

    public func getValue(for attribute: String, from data: Data?, prefferedArrayRepresentation: PrefferedArrayRepresentation = .array) throws -> UpdateValue {
        guard let attr = attributes[attribute] else {
            throw SchemeDescriptorError.unknownAttribute
        }
        return try attr.asUpdateValue(from: data, prefferedArrayRepresentation: prefferedArrayRepresentation)
    }

    public func getData(for attribute: String, value: UpdateValue) throws -> Data? {
        guard let attr = attributes[attribute] else {
            throw SchemeDescriptorError.unknownAttribute
        }
        return try attr.getData(for: value)
    }
}
