//
//  SchemeDescribable.swift
//  
//
//  Created by Filip Klembara on 29/02/2020.
//

import Foundation

public protocol SchemeDescribable: SchemeOptionalAttribute {
    init()
    static var schemeIgnoredProperties: [String] { get }
}

public protocol SchemeAttribute {
    var attributeType: ClassAttributesDescription.AttributeType { get }
    var ignore: Bool { get }
}

extension SchemeAttribute {
    public var ignore: Bool { false }
}

public protocol SchemeOptionalAttribute {
    static var optionalAttributeType: ClassAttributesDescription.AttributeType { get }
}

extension Int: SchemeAttribute, SchemeOptionalAttribute {
    public var attributeType: ClassAttributesDescription.AttributeType { .int }
    public static var optionalAttributeType: ClassAttributesDescription.AttributeType { .optionalInt }
}

extension UInt: SchemeAttribute, SchemeOptionalAttribute {
    public var attributeType: ClassAttributesDescription.AttributeType { .uint }
    public static var optionalAttributeType: ClassAttributesDescription.AttributeType { .optionalUInt }
}

extension String: SchemeAttribute, SchemeOptionalAttribute {
    public var attributeType: ClassAttributesDescription.AttributeType { .string }
    public static var optionalAttributeType: ClassAttributesDescription.AttributeType { .optionalString }
}

extension Data: SchemeAttribute, SchemeOptionalAttribute {
    public var attributeType: ClassAttributesDescription.AttributeType { .data }
    public static var optionalAttributeType: ClassAttributesDescription.AttributeType { .optionalData }
}

extension Bool: SchemeAttribute, SchemeOptionalAttribute {
    public var attributeType: ClassAttributesDescription.AttributeType { .bool }
    public static var optionalAttributeType: ClassAttributesDescription.AttributeType { .optionalBool }
}

extension Double: SchemeAttribute, SchemeOptionalAttribute {
    public var attributeType: ClassAttributesDescription.AttributeType { .double }
    public static var optionalAttributeType: ClassAttributesDescription.AttributeType { .optionalDouble }
}


extension Date: SchemeAttribute, SchemeOptionalAttribute {
    public var attributeType: ClassAttributesDescription.AttributeType { .date }
    public static var optionalAttributeType: ClassAttributesDescription.AttributeType { .optionalDate }
}

extension Optional: SchemeAttribute where Wrapped: SchemeOptionalAttribute {
    public var attributeType: ClassAttributesDescription.AttributeType {
        Wrapped.optionalAttributeType
    }
}

extension Array: SchemeAttribute where Element: SchemeDescribable {
    public var attributeType: ClassAttributesDescription.AttributeType { .array(className: Element.className) }
}

extension Array where Element == SchemeDescribable.Type {
    public var scheme: MeerkatSchemeDescriptor {
        MeerkatSchemeDescriptor(scheme: map { $0.schemeDescription })
    }
}

extension SchemeDescribable {
    public static var optionalAttributeType: ClassAttributesDescription.AttributeType { .optionalObject(className: className) }

    static var className: String {
        let mirror = Mirror(reflecting: Self.init())
        return "\(mirror.subjectType)"
    }

    public static var schemeIgnoredProperties: [String] {
        []
    }

    public static var schemeDescription: ClassAttributesDescription {
        let mirror = Mirror(reflecting: Self.init())
        let className = "\(mirror.subjectType)"
        var attributes = [String: ClassAttributesDescription.AttributeType]()
        if let supClassMirror = mirror.superclassMirror {
            if let supClass = supClassMirror.subjectType as? SchemeDescribable.Type {
                let supAttributes = supClass.schemeDescription.attributes
                attributes.merge(supAttributes) { a, b in b }
            }
        }
        mirror.children.forEach { child in
            let label = child.label!
            guard !schemeIgnoredProperties.contains(label) else { return }
            let value = child.value as! SchemeAttribute
            guard !value.ignore else { return }
            attributes[label] = value.attributeType
        }
        return ClassAttributesDescription(className: className, attributes: attributes)
    }
}
