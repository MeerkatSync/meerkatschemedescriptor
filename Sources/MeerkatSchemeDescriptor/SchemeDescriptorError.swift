//
//  SchemeDescriptorError.swift
//  
//
//  Created by Filip Klembara on 18/02/2020.
//

enum SchemeDescriptorError: Error {
    case wrongType
    case unknownClass
    case notOptional
    case unknownAttribute
}
