// swift-tools-version:5.1

import PackageDescription

let package = Package(
    name: "MeerkatSchemeDescriptor",
    platforms: [
        .iOS("13.0"),
        .macOS("10.15")
    ],
    products: [
        .library(
            name: "MeerkatSchemeDescriptor",
            targets: ["MeerkatSchemeDescriptor"]),
    ],
    dependencies: [
        .package(url: "https://gitlab.com/MeerkatSync/meerkatcore", from: "1.0.0"),
    ],
    targets: [
        .target(
            name: "MeerkatSchemeDescriptor",
            dependencies: ["MeerkatCore"]),
        .testTarget(
            name: "MeerkatSchemeDescriptorTests",
            dependencies: ["MeerkatSchemeDescriptor"]),
    ]
)
