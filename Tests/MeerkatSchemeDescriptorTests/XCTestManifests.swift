import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(MeerkatSchemeDescriptorTests.allTests),
    ]
}
#endif
