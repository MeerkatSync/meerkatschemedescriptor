import XCTest
@testable import MeerkatSchemeDescriptor

final class MeerkatSchemeDescriptorTests: XCTestCase {
    func testPersonDescription() {
        struct Person: SchemeDescribable {
            var name: String = ""
            var age: UInt? = nil
        }

        let desc = Person.schemeDescription
        XCTAssertEqual(desc.className, "Person")
        XCTAssertEqual(desc.attributes.count, 2)

        XCTAssertNotNil(desc.attributes["name"])
        XCTAssertNotNil(desc.attributes["age"])

        guard case let (at1?, at2?) = (desc.attributes["name"], desc.attributes["age"]) else {
            XCTFail()
            return
        }

        XCTAssertFalse(at1.isOptional)
        XCTAssertFalse(at1.isRelation)
        XCTAssertTrue(at2.isOptional)
        XCTAssertFalse(at2.isRelation)

        guard case .string = at1 else {
            XCTFail()
            return
        }

        guard case .optionalUInt = at2 else {
            XCTFail()
            return
        }
    }
    func testPersonChildDescription() {
        class SuperPerson: SchemeDescribable {
            var name: String = ""
            required init() {
                name = ""
            }
        }
        class Person: SuperPerson {
            var age: UInt? = nil
        }

        let desc = Person.schemeDescription
        XCTAssertEqual(desc.className, "Person")
        XCTAssertEqual(desc.attributes.count, 2)

        XCTAssertNotNil(desc.attributes["name"])
        XCTAssertNotNil(desc.attributes["age"])

        guard case let (at1?, at2?) = (desc.attributes["name"], desc.attributes["age"]) else {
            XCTFail()
            return
        }

        XCTAssertFalse(at1.isOptional)
        XCTAssertFalse(at1.isRelation)
        XCTAssertTrue(at2.isOptional)
        XCTAssertFalse(at2.isRelation)

        guard case .string = at1 else {
            XCTFail()
            return
        }

        guard case .optionalUInt = at2 else {
            XCTFail()
            return
        }
    }

    func testPersonWithDogDescription() {
        struct Person: SchemeDescribable {
            var name: String = ""
            var age: UInt? = nil
            var dogs: [Dog] = []
        }

        struct Dog: SchemeDescribable {
            var owner: Person? = nil
            var name: String = ""
        }

        let desc = Person.schemeDescription
        XCTAssertEqual(desc.className, "Person")
        XCTAssertEqual(desc.attributes.count, 3)

        XCTAssertNotNil(desc.attributes["name"])
        XCTAssertNotNil(desc.attributes["age"])
        XCTAssertNotNil(desc.attributes["dogs"])

        guard case let (at1?, at2?, at3?) = (desc.attributes["name"], desc.attributes["age"], desc.attributes["dogs"]) else {
            XCTFail()
            return
        }

        XCTAssertFalse(at1.isOptional)
        XCTAssertFalse(at1.isRelation)
        XCTAssertTrue(at2.isOptional)
        XCTAssertFalse(at2.isRelation)
        XCTAssertFalse(at3.isOptional)
        XCTAssertTrue(at3.isRelation)

        guard case .string = at1 else {
            XCTFail()
            return
        }

        guard case .optionalUInt = at2 else {
            XCTFail()
            return
        }

        guard case let .array(clsName) = at3 else {
            XCTFail()
            return
        }
        XCTAssertEqual(clsName, "Dog")
    }

    func testDogWithDescription() {
        struct Person: SchemeDescribable {
            var name: String = ""
            var age: UInt? = nil
            var dogs: [Dog] = []
        }

        struct Dog: SchemeDescribable {
            var owner: Person? = nil
            var name: String = ""
        }



        let desc = Dog.schemeDescription
        XCTAssertEqual(desc.className, "Dog")
        XCTAssertEqual(desc.attributes.count, 2)

        XCTAssertNotNil(desc.attributes["name"])
        XCTAssertNotNil(desc.attributes["owner"])

        guard case let (at1?, at2?) = (desc.attributes["name"], desc.attributes["owner"]) else {
            XCTFail()
            return
        }

        XCTAssertFalse(at1.isOptional)
        XCTAssertFalse(at1.isRelation)
        XCTAssertTrue(at2.isOptional)
        XCTAssertTrue(at2.isRelation)

        guard case .string = at1 else {
            XCTFail()
            return
        }

        guard case let .optionalObject(className: clsName) = at2 else {
            XCTFail()
            return
        }
        XCTAssertEqual(clsName, "Person")
    }

    func testIgnoreProperties() {
        struct Somthing {

        }
        struct Person: SchemeDescribable {
            var name: String = ""
            var age: UInt? = nil
            var dogs: [Dog] = []
        }

        struct Dog: SchemeDescribable {
            var owner: Person? = nil
            var name: String = ""
            var age = 0.0
            var ig = Somthing()
            static var schemeIgnoredProperties: [String] {
                ["ig", "owner"]
            }
        }

        let desc = Dog.schemeDescription
        XCTAssertEqual(desc.className, "Dog")
        XCTAssertEqual(desc.attributes.count, 2)

        XCTAssertNotNil(desc.attributes["name"])
        XCTAssertNotNil(desc.attributes["age"])
        XCTAssertNil(desc.attributes["ig"])
        XCTAssertNil(desc.attributes["owner"])

        guard case let (at1?, at2?) = (desc.attributes["name"], desc.attributes["age"]) else {
            XCTFail()
            return
        }

        XCTAssertFalse(at1.isOptional)
        XCTAssertFalse(at1.isRelation)
        XCTAssertFalse(at2.isOptional)
        XCTAssertFalse(at2.isRelation)

        guard case .string = at1 else {
            XCTFail()
            return
        }

        guard case .double = at2 else {
            XCTFail()
            return
        }
    }

    func testIgnorePropertyDescription() {
        struct Ignored: SchemeAttribute {
            var ignore: Bool = true
            var attributeType: ClassAttributesDescription.AttributeType {
                fatalError()
            }
        }
        struct Person: SchemeDescribable {
            var name: String = ""
            var age: UInt? = nil
            var dogs: [Dog] = []
        }

        struct Dog: SchemeDescribable {
            var owner: Person? = nil
            var name: String = ""
            var ig = Ignored()
        }

        let desc = Dog.schemeDescription
        XCTAssertEqual(desc.className, "Dog")
        XCTAssertEqual(desc.attributes.count, 2)

        XCTAssertNotNil(desc.attributes["name"])
        XCTAssertNotNil(desc.attributes["owner"])

        guard case let (at1?, at2?) = (desc.attributes["name"], desc.attributes["owner"]) else {
            XCTFail()
            return
        }

        XCTAssertFalse(at1.isOptional)
        XCTAssertFalse(at1.isRelation)
        XCTAssertTrue(at2.isOptional)
        XCTAssertTrue(at2.isRelation)

        guard case .string = at1 else {
            XCTFail()
            return
        }

        guard case let .optionalObject(className: clsName) = at2 else {
            XCTFail()
            return
        }
        XCTAssertEqual(clsName, "Person")
    }

    func testStringValue() throws {
        let className = All.className

        var scheme = MeerkatSchemeDescriptor(scheme: [All.Something.schemeDescription])

        scheme.addClass(All.schemeDescription)

        let d1 = try scheme.getData(for: "string", value: .string("Bobby"), in: className)
        XCTAssertNotNil(d1)
        let v1 = try scheme.getValue(for: "string", from: d1, in: className)
        guard case .string(let s1) = v1 else { return XCTFail("\(v1)") }
        XCTAssertEqual(s1, "Bobby")

        let d2 = try scheme.getData(for: "string", value: .string("Evička"), in: className)
        XCTAssertNotNil(d2)
        let v2 = try scheme.getValue(for: "string", from: d2, in: className)
        guard case .string(let s2) = v2 else { return XCTFail("\(v2)") }
        XCTAssertEqual(s2, "Evička")

        XCTAssertThrowsError(try scheme.getData(for: "string", value: .nil, in: className))
    }


    func testOptionalStringValue() throws {
        let className = All.className

        var scheme = MeerkatSchemeDescriptor(scheme: [All.Something.schemeDescription])

        scheme.addClass(All.schemeDescription)

        let d1 = try scheme.getData(for: "optionalString", value: .string("Bobby"), in: className)
        XCTAssertNotNil(d1)
        let v1 = try scheme.getValue(for: "optionalString", from: d1, in: className)
        guard case .string(let s1) = v1 else { return XCTFail("\(v1)") }
        XCTAssertEqual(s1, "Bobby")

        let d2 = try scheme.getData(for: "optionalString", value: .string("Evička"), in: className)
        XCTAssertNotNil(d2)
        let v2 = try scheme.getValue(for: "optionalString", from: d2, in: className)
        guard case .string(let s2) = v2 else { return XCTFail("\(v2)") }
        XCTAssertEqual(s2, "Evička")

        let d3 = try scheme.getData(for: "optionalString", value: .nil, in: className)
        XCTAssertNil(d3)
        let v3 = try scheme.getValue(for: "optionalString", from: d3, in: className)
        guard case .nil = v3 else { return XCTFail("\(v3)") }
    }

    func testIntValue() throws {
        let className = All.className

        var scheme = MeerkatSchemeDescriptor(scheme: [All.Something.schemeDescription])

        scheme.addClass(All.schemeDescription)

        let d1 = try scheme.getData(for: "int", value: .int(3), in: className)
        XCTAssertNotNil(d1)
        let v1 = try scheme.getValue(for: "int", from: d1, in: className)
        guard case .int(let s1) = v1 else { return XCTFail("\(v1)") }
        XCTAssertEqual(s1, 3)

        let d2 = try scheme.getData(for: "int", value: .int(-53252213421), in: className)
        XCTAssertNotNil(d2)
        let v2 = try scheme.getValue(for: "int", from: d2, in: className)
        guard case .int(let s2) = v2 else { return XCTFail("\(v2)") }
        XCTAssertEqual(s2, -53252213421)

        let d3 = try scheme.getData(for: "int", value: .int(0), in: className)
        XCTAssertNotNil(d2)
        let v3 = try scheme.getValue(for: "int", from: d3, in: className)
        guard case .int(let s3) = v3 else { return XCTFail("\(v3)") }
        XCTAssertEqual(s3, 0)

        XCTAssertThrowsError(try scheme.getData(for: "int", value: .nil, in: className))
    }

    func testOptionalIntValue() throws {
        let className = All.className

        var scheme = MeerkatSchemeDescriptor(scheme: [All.Something.schemeDescription])

        scheme.addClass(All.schemeDescription)

        let d1 = try scheme.getData(for: "optionalInt", value: .int(3), in: className)
        XCTAssertNotNil(d1)
        let v1 = try scheme.getValue(for: "optionalInt", from: d1, in: className)
        guard case .int(let s1) = v1 else { return XCTFail("\(v1)") }
        XCTAssertEqual(s1, 3)

        let d2 = try scheme.getData(for: "optionalInt", value: .int(-53252213421), in: className)
        XCTAssertNotNil(d2)
        let v2 = try scheme.getValue(for: "optionalInt", from: d2, in: className)
        guard case .int(let s2) = v2 else { return XCTFail("\(v2)") }
        XCTAssertEqual(s2, -53252213421)

        let d3 = try scheme.getData(for: "optionalInt", value: .int(0), in: className)
        XCTAssertNotNil(d2)
        let v3 = try scheme.getValue(for: "optionalInt", from: d3, in: className)
        guard case .int(let s3) = v3 else { return XCTFail("\(v3)") }
        XCTAssertEqual(s3, 0)

        let d4 = try scheme.getData(for: "optionalInt", value: .nil, in: className)
        XCTAssertNil(d4)
        let v4 = try scheme.getValue(for: "optionalInt", from: d4, in: className)
        guard case .nil = v4 else { return XCTFail("\(v4)") }
    }

    func testDataValue() throws {
        let className = All.className

        var scheme = MeerkatSchemeDescriptor(scheme: [All.Something.schemeDescription])

        scheme.addClass(All.schemeDescription)
        let d = "something".data(using: .utf8)!
        let d1 = try scheme.getData(for: "data", value: .data(d), in: className)
        XCTAssertNotNil(d1)
        let v1 = try scheme.getValue(for: "data", from: d1, in: className)
        guard case .data(let s1) = v1 else { return XCTFail("\(v1)") }
        XCTAssertEqual(s1, d)

        let d2 = try scheme.getData(for: "data", value: .data(Data()), in: className)
        XCTAssertNotNil(d2)
        let v2 = try scheme.getValue(for: "data", from: d2, in: className)
        guard case .data(let s2) = v2 else { return XCTFail("\(v2)") }
        XCTAssertEqual(s2, Data())

        XCTAssertThrowsError(try scheme.getData(for: "data", value: .nil, in: className))
    }

    func testOptionalDataValue() throws {
        let className = All.className

        var scheme = MeerkatSchemeDescriptor(scheme: [All.Something.schemeDescription])

        scheme.addClass(All.schemeDescription)
        let d = "something".data(using: .utf8)!
        let d1 = try scheme.getData(for: "optionalData", value: .data(d), in: className)
        XCTAssertNotNil(d1)
        let v1 = try scheme.getValue(for: "optionalData", from: d1, in: className)
        guard case .data(let s1) = v1 else { return XCTFail("\(v1)") }
        XCTAssertEqual(s1, d)

        let d2 = try scheme.getData(for: "optionalData", value: .data(Data()), in: className)
        XCTAssertNotNil(d2)
        let v2 = try scheme.getValue(for: "optionalData", from: d2, in: className)
        guard case .data(let s2) = v2 else { return XCTFail("\(v2)") }
        XCTAssertEqual(s2, Data())

        let d3 = try scheme.getData(for: "optionalData", value: .nil, in: className)
        XCTAssertNil(d3)
        let v3 = try scheme.getValue(for: "optionalData", from: d3, in: className)
        guard case .nil = v3 else { return XCTFail("\(v3)") }
    }

    func testBoolValue() throws {
        let className = All.className

        var scheme = MeerkatSchemeDescriptor(scheme: [All.Something.schemeDescription])

        scheme.addClass(All.schemeDescription)
        let d1 = try scheme.getData(for: "bool", value: .bool(true), in: className)
        XCTAssertNotNil(d1)
        let v1 = try scheme.getValue(for: "bool", from: d1, in: className)
        guard case .bool(let s1) = v1 else { return XCTFail("\(v1)") }
        XCTAssertEqual(s1, true)

        let d2 = try scheme.getData(for: "bool", value: .bool(false), in: className)
        XCTAssertNotNil(d2)
        let v2 = try scheme.getValue(for: "bool", from: d2, in: className)
        guard case .bool(let s2) = v2 else { return XCTFail("\(v2)") }
        XCTAssertEqual(s2, false)

        XCTAssertThrowsError(try scheme.getData(for: "bool", value: .nil, in: className))
    }

    func testOptionalBoolValue() throws {
        let className = All.className

        var scheme = MeerkatSchemeDescriptor(scheme: [All.Something.schemeDescription])

        scheme.addClass(All.schemeDescription)
        let d1 = try scheme.getData(for: "optionalBool", value: .bool(true), in: className)
        XCTAssertNotNil(d1)
        let v1 = try scheme.getValue(for: "optionalBool", from: d1, in: className)
        guard case .bool(let s1) = v1 else { return XCTFail("\(v1)") }
        XCTAssertEqual(s1, true)

        let d2 = try scheme.getData(for: "optionalBool", value: .bool(false), in: className)
        XCTAssertNotNil(d2)
        let v2 = try scheme.getValue(for: "optionalBool", from: d2, in: className)
        guard case .bool(let s2) = v2 else { return XCTFail("\(v2)") }
        XCTAssertEqual(s2, false)

        let d3 = try scheme.getData(for: "optionalBool", value: .nil, in: className)
        XCTAssertNil(d3)
        let v3 = try scheme.getValue(for: "optionalBool", from: d3, in: className)
        guard case .nil = v3 else { return XCTFail("\(v3)") }
    }

    func testUIntValue() throws {
        let className = All.className

        var scheme = MeerkatSchemeDescriptor(scheme: [All.Something.schemeDescription])

        scheme.addClass(All.schemeDescription)

        let d1 = try scheme.getData(for: "uint", value: .uint(3), in: className)
        XCTAssertNotNil(d1)
        let v1 = try scheme.getValue(for: "uint", from: d1, in: className)
        guard case .uint(let s1) = v1 else { return XCTFail("\(v1)") }
        XCTAssertEqual(s1, 3)

        let d2 = try scheme.getData(for: "uint", value: .uint(39524523521), in: className)
        XCTAssertNotNil(d2)
        let v2 = try scheme.getValue(for: "uint", from: d2, in: className)
        guard case .uint(let s2) = v2 else { return XCTFail("\(v2)") }
        XCTAssertEqual(s2, 39524523521)

        let d3 = try scheme.getData(for: "uint", value: .uint(0), in: className)
        XCTAssertNotNil(d2)
        let v3 = try scheme.getValue(for: "uint", from: d3, in: className)
        guard case .uint(let s3) = v3 else { return XCTFail("\(v3)") }
        XCTAssertEqual(s3, 0)

        XCTAssertThrowsError(try scheme.getData(for: "uint", value: .nil, in: className))
    }

    func testOptionalUIntValue() throws {
        let className = All.className

        var scheme = MeerkatSchemeDescriptor(scheme: [All.Something.schemeDescription])

        scheme.addClass(All.schemeDescription)

        let d1 = try scheme.getData(for: "optionalUInt", value: .uint(3), in: className)
        XCTAssertNotNil(d1)
        let v1 = try scheme.getValue(for: "optionalUInt", from: d1, in: className)
        guard case .uint(let s1) = v1 else { return XCTFail("\(v1)") }
        XCTAssertEqual(s1, 3)

        let d2 = try scheme.getData(for: "optionalUInt", value: .uint(39524523521), in: className)
        XCTAssertNotNil(d2)
        let v2 = try scheme.getValue(for: "optionalUInt", from: d2, in: className)
        guard case .uint(let s2) = v2 else { return XCTFail("\(v2)") }
        XCTAssertEqual(s2, 39524523521)

        let d3 = try scheme.getData(for: "optionalUInt", value: .uint(0), in: className)
        XCTAssertNotNil(d2)
        let v3 = try scheme.getValue(for: "optionalUInt", from: d3, in: className)
        guard case .uint(let s3) = v3 else { return XCTFail("\(v3)") }
        XCTAssertEqual(s3, 0)

        let d4 = try scheme.getData(for: "optionalUInt", value: .nil, in: className)
        XCTAssertNil(d4)
        let v4 = try scheme.getValue(for: "optionalUInt", from: d4, in: className)
        guard case .nil = v4 else { return XCTFail("\(v4)") }
    }

    func testDoubleValue() throws {
        let className = All.className

        var scheme = MeerkatSchemeDescriptor(scheme: [All.Something.schemeDescription])

        scheme.addClass(All.schemeDescription)

        let d1 = try scheme.getData(for: "double", value: .double(34512.5115346), in: className)
        XCTAssertNotNil(d1)
        let v1 = try scheme.getValue(for: "double", from: d1, in: className)
        guard case .double(let s1) = v1 else { return XCTFail("\(v1)") }
        XCTAssertEqual(s1, 34512.5115346)

        let d2 = try scheme.getData(for: "double", value: .double(4.12351), in: className)
        XCTAssertNotNil(d2)
        let v2 = try scheme.getValue(for: "double", from: d2, in: className)
        guard case .double(let s2) = v2 else { return XCTFail("\(v2)") }
        XCTAssertEqual(s2, 4.12351)

        let d3 = try scheme.getData(for: "double", value: .double(0), in: className)
        XCTAssertNotNil(d2)
        let v3 = try scheme.getValue(for: "double", from: d3, in: className)
        guard case .double(let s3) = v3 else { return XCTFail("\(v3)") }
        XCTAssertEqual(s3, 0)

        XCTAssertThrowsError(try scheme.getData(for: "double", value: .nil, in: className))
    }

    func testOptionalDoubleValue() throws {
        let className = All.className

        var scheme = MeerkatSchemeDescriptor(scheme: [All.Something.schemeDescription])

        scheme.addClass(All.schemeDescription)

        let d1 = try scheme.getData(for: "optionalDouble", value: .double(34512.5115346), in: className)
        XCTAssertNotNil(d1)
        let v1 = try scheme.getValue(for: "optionalDouble", from: d1, in: className)
        guard case .double(let s1) = v1 else { return XCTFail("\(v1)") }
        XCTAssertEqual(s1, 34512.5115346)

        let d2 = try scheme.getData(for: "optionalDouble", value: .double(4.12351), in: className)
        XCTAssertNotNil(d2)
        let v2 = try scheme.getValue(for: "optionalDouble", from: d2, in: className)
        guard case .double(let s2) = v2 else { return XCTFail("\(v2)") }
        XCTAssertEqual(s2, 4.12351)

        let d3 = try scheme.getData(for: "optionalDouble", value: .double(0), in: className)
        XCTAssertNotNil(d2)
        let v3 = try scheme.getValue(for: "optionalDouble", from: d3, in: className)
        guard case .double(let s3) = v3 else { return XCTFail("\(v3)") }
        XCTAssertEqual(s3, 0)

        let d4 = try scheme.getData(for: "optionalDouble", value: .nil, in: className)
        XCTAssertNil(d4)
        let v4 = try scheme.getValue(for: "optionalDouble", from: d4, in: className)
        guard case .nil = v4 else { return XCTFail("\(v4)") }
    }

    func testDateValue() throws {
        let className = All.className

        var scheme = MeerkatSchemeDescriptor(scheme: [All.Something.schemeDescription])

        let date1 = Date().addingTimeInterval(-523134)
        scheme.addClass(All.schemeDescription)
        let d1 = try scheme.getData(for: "date", value: .date(date1), in: className)
        XCTAssertNotNil(d1)
        let v1 = try scheme.getValue(for: "date", from: d1, in: className)
        guard case .date(let s1) = v1 else { return XCTFail("\(v1)") }
        XCTAssertEqual("\(s1)", "\(date1)")

        let date2 = Date().addingTimeInterval(5231351)
        let d2 = try scheme.getData(for: "date", value: .date(date2), in: className)
        XCTAssertNotNil(d2)
        let v2 = try scheme.getValue(for: "date", from: d2, in: className)
        guard case .date(let s2) = v2 else { return XCTFail("\(v2)") }
        XCTAssertEqual("\(s2)", "\(date2)")

        XCTAssertThrowsError(try scheme.getData(for: "date", value: .nil, in: className))
    }

    func testOptionalDateValue() throws {
        let className = All.className

        var scheme = MeerkatSchemeDescriptor(scheme: [All.Something.schemeDescription])

        let date1 = Date().addingTimeInterval(-523134)
        scheme.addClass(All.schemeDescription)
        let d1 = try scheme.getData(for: "optionalDate", value: .date(date1), in: className)
        XCTAssertNotNil(d1)
        let v1 = try scheme.getValue(for: "optionalDate", from: d1, in: className)
        guard case .date(let s1) = v1 else { return XCTFail("\(v1)") }
        XCTAssertEqual("\(s1)", "\(date1)")

        let date2 = Date().addingTimeInterval(5231351)
        let d2 = try scheme.getData(for: "optionalDate", value: .date(date2), in: className)
        XCTAssertNotNil(d2)
        let v2 = try scheme.getValue(for: "optionalDate", from: d2, in: className)
        guard case .date(let s2) = v2 else { return XCTFail("\(v2)") }
        XCTAssertEqual("\(s2)", "\(date2)")

        let d3 = try scheme.getData(for: "optionalDate", value: .nil, in: className)
        XCTAssertNil(d3)
        let v3 = try scheme.getValue(for: "optionalDate", from: d3, in: className)
        guard case .nil = v3 else { return XCTFail("\(v3)") }
    }

    func testOptionalObjectValue() throws {
        let className = All.className

        var scheme = MeerkatSchemeDescriptor(scheme: [All.Something.schemeDescription])

        scheme.addClass(All.schemeDescription)
        let id1 = UUID().uuidString
        let d1 = try scheme.getData(for: "optionalObject", value: .object(id: id1), in: className)
        XCTAssertNotNil(d1)
        let v1 = try scheme.getValue(for: "optionalObject", from: d1, in: className)
        guard case .object(id: let s1) = v1 else { return XCTFail("\(v1)") }
        XCTAssertEqual(s1, id1)

        let id2 = UUID().uuidString
        let d2 = try scheme.getData(for: "optionalObject", value: .object(id: id2), in: className)
        XCTAssertNotNil(d2)
        let v2 = try scheme.getValue(for: "optionalObject", from: d2, in: className)
        guard case .object(id: let s2) = v2 else { return XCTFail("\(v2)") }
        XCTAssertEqual(s2, id2)

        let d3 = try scheme.getData(for: "optionalObject", value: .nil, in: className)
        XCTAssertNil(d3)
        let v3 = try scheme.getValue(for: "optionalObject", from: d3, in: className)
        guard case .nil = v3 else { return XCTFail("\(v3)") }
    }


    func testArrayValue() throws {
        let className = All.className

        var scheme = MeerkatSchemeDescriptor(scheme: [All.Something.schemeDescription])

        scheme.addClass(All.schemeDescription)

        let d1 = try scheme.getData(for: "array", value: .array([]), in: className)
        XCTAssertNotNil(d1)
        let v1 = try scheme.getValue(for: "array", from: d1, in: className)
        guard case .array(let s1) = v1 else { return XCTFail("\(v1)") }
        XCTAssertEqual(s1, [])

        let a1 = [UUID(), UUID(), UUID()].map { $0.uuidString }
        let d2 = try scheme.getData(for: "array", value: .array(a1), in: className)
        XCTAssertNotNil(d2)
        let v2 = try scheme.getValue(for: "array", from: d2, in: className)
        guard case .array(let s2) = v2 else { return XCTFail("\(v2)") }
        XCTAssertEqual(s2, a1)

        let a2 = [UUID(), UUID(), UUID(), UUID(), UUID(), UUID(), UUID(), UUID(), UUID(), UUID()].map { $0.uuidString }
        let d3 = try scheme.getData(for: "array", value: .array(a2), in: className)
        XCTAssertNotNil(d2)
        let v3 = try scheme.getValue(for: "array", from: d3, in: className)
        guard case .array(let s3) = v3 else { return XCTFail("\(v3)") }
        XCTAssertEqual(s3, a2)

        XCTAssertThrowsError(try scheme.getData(for: "array", value: .nil, in: className))
    }

    struct All: SchemeDescribable {

        struct Something: SchemeDescribable {

        }
        var string = ""
        var int = 0
        var data = Data()
        var bool = true
        var uint: UInt = 0
        var double = 0.0
        var date = Date()
        var optionalString: String? = nil
        var optionalInt: Int? = nil
        var optionalData: Data? = nil
        var optionalBool: Bool? = nil
        var optionalUInt: UInt? = nil
        var optionalDouble: Double? = nil
        var optionalObject: Something?
        var optionalDate: Date? = nil
        var array: [Something] = []
    }


    static var allTests = [
        ("testPersonDescription", testPersonDescription),
        ("testPersonChildDescription", testPersonChildDescription),
        ("testPersonWithDogDescription", testPersonWithDogDescription),
        ("testDogWithDescription", testDogWithDescription),
        ("testIgnorePropertyDescription", testIgnorePropertyDescription),
        ("testStringValue", testStringValue),
        ("testOptionalStringValue", testOptionalStringValue),
        ("testIntValue", testIntValue),
        ("testOptionalIntValue", testOptionalIntValue),
        ("testDataValue", testDataValue),
        ("testOptionalDataValue", testOptionalDataValue),
        ("testBoolValue", testBoolValue),
        ("testOptionalBoolValue", testOptionalBoolValue),
        ("testUIntValue", testUIntValue),
        ("testOptionalUIntValue", testOptionalUIntValue),
        ("testDoubleValue", testDoubleValue),
        ("testOptionalDoubleValue", testOptionalDoubleValue),
        ("testDateValue", testDateValue),
        ("testOptionalDateValue", testOptionalDateValue),
        ("testOptionalObjectValue", testOptionalObjectValue),
        ("testArrayValue", testArrayValue),
    ]
}
