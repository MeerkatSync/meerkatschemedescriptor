import XCTest

import MeerkatSchemeDescriptorTests

var tests = [XCTestCaseEntry]()
tests += MeerkatSchemeDescriptorTests.allTests()
XCTMain(tests)
